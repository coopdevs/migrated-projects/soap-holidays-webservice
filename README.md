# SOAP Postman collection example: Holidays webservice

## Description

This collection is meant to be run with Postman (GUI) and Newman (CLI). It uses a SOAP API that calculates holiday dates for English-speaking countries of Europe and North America.

## Purpose

The goal of this collection is to learn how to use Postman to build a SOAP client. Postman is focused on REST, JSON, and JavaScript, so there's a couple of obstacles to be sorted out.

## Documentation

* [Postman docs on SOAP](https://blog.getpostman.com/2014/08/22/making-soap-requests-using-postman/)
* [Postman test scripts examples](https://learning.getpostman.com/docs/postman/scripts/test_examples)
* [Reference of Chai testing library](https://www.chaijs.com/api/bdd/)

## Newman installing

1. Install npm if missing. Check it with `npm --version`
2. Install newman globally with `npm install -g newman`

## How to run with newman

`newman run 'Example postman collection.json'`
